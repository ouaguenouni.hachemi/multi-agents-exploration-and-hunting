
# Multi-agent exploration and hunting

This project was developed during the course "Multi-agent systems" of the master of Distributed Agents, Operational Research, Robotics, Interaction and Decision of Sorbonne University.

This project contains a modeling of cognitive agents in JADE that communicate information about their positions and the topology of the map by exchanging RDF graphs using a SPARQL-based protocol.

This information helps them to track the Wumpus in a graphed environment, and to develop distributed strategies based on Policy Evaluation planning.
